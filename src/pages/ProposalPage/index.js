import React from 'react'
import {PageTitle, BackTray, SubTitle, InfoTitle} from './../../components'

import style from './style.css'

const proposal = {
  id: 1,
  type: 'Purchase Contract Proxy',
  date: '2021-02-04',
  fullName: 'James Tan',
  status: 'review',
}
const {id, type, date, fullName, status} = proposal;
export const ProposalPage = () => {


  return(
      <div className="main__container">
        <div className="proposal-title-buttons">
          <PageTitle name={'proposal'} />
          <div className="proposal-buttons">
            <div className="reject-button">Reject</div>
            <div className="create-issue-button">Create Issue</div>
          </div>
        </div>
        <BackTray />
        <div className="main-card-block">
          <div className="proposal-info-block">
            <div className="proposal-info-status">
              <SubTitle name={`${type}`}/>
              <div className='proposal-status grey-dark fs-12 ml-10'>
                <span className={status === 'review' ? 'review-status' : 'complete-status'}>{status === 'review' ? 'For review' : 'Complete'}</span>
              </div>
            </div>
            <div className="proposal-description">
              Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.
            </div>
          </div>
          <div className="proposal-info-block">
            <div className="proposal-info-property">
              <div className="proposal-info-title">ID</div>
              {id}
            </div>
            <div className="proposal-info-property">
              <div className="proposal-info-title">Date</div>
              {date}
            </div>
            <div className="proposal-info-property">
              <div className="proposal-info-title">manager</div>
              {fullName}
            </div>
          </div>
          <div className="proposal-info-block">
            <div className="proposal-info-property">
              <div className="proposal-info-title">Autorization document</div>
              <div className="proposal-info-document">
                <span><svg width="24" height="24" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M14.3962 9.61774C14.6765 9.33748 14.6764 8.88306 14.396 8.60294C14.1158 8.32303 13.6618 8.32312 13.3817 8.60314L8.81376 13.1702C8.61389 13.3702 8.45536 13.6075 8.34721 13.8687C8.23907 14.1299 8.18343 14.4099 8.18348 14.6926C8.18352 14.9753 8.23925 15.2552 8.34748 15.5164C8.45571 15.7775 8.61432 16.0148 8.81426 16.2147C9.0142 16.4146 9.25155 16.5731 9.51275 16.6813C9.77396 16.7894 10.0539 16.845 10.3366 16.845C10.6193 16.845 10.8993 16.7892 11.1604 16.681C11.4216 16.5728 11.6589 16.4142 11.8588 16.2142L17.9488 10.1252C18.6216 9.45223 18.9996 8.53951 18.9995 7.58786C18.9994 6.63621 18.6212 5.72357 17.9483 5.05071C17.2753 4.37786 16.3626 3.99991 15.4109 4C14.4593 4.00009 13.5466 4.37823 12.8738 5.05121L6.47976 11.4442L6.46576 11.4572C4.51076 13.4122 4.51076 16.5802 6.46576 18.5342C8.42076 20.4882 11.5888 20.4882 13.5438 18.5342L13.5568 18.5202L13.5578 18.5212L17.4151 14.6647C17.6954 14.3845 17.6953 13.93 17.4149 13.6499C17.1348 13.37 16.6808 13.3701 16.4007 13.65L12.5298 17.5192C11.1378 18.9112 8.87376 18.9112 7.48176 17.5192C7.14972 17.1864 6.88663 16.7914 6.70758 16.3568C6.52852 15.9221 6.43702 15.4564 6.43832 14.9863C6.43962 14.5162 6.5337 14.051 6.71516 13.6174C6.89661 13.1837 7.16189 12.7901 7.49576 12.4592L7.49476 12.4582L13.8898 6.06521C14.7288 5.22521 16.0948 5.22521 16.9348 6.06521C17.7748 6.90521 17.7738 8.27021 16.9348 9.10921L10.8448 15.1982C10.7085 15.3236 10.529 15.3914 10.3439 15.3876C10.1587 15.3838 9.98224 15.3086 9.85123 15.1777C9.72023 15.0468 9.64485 14.8704 9.64085 14.6853C9.63685 14.5001 9.70453 14.3206 9.82976 14.1842L14.3962 9.61774Z" fill="#8FA4B5"/>
                </svg></span>
                Document.txt
              </div>
            </div>
            <div className="proposal-info-property">
              <div className="proposal-info-title">Additional document</div>
              <div className="proposal-info-document">
                <span><svg width="24" height="24" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M14.3962 9.61774C14.6765 9.33748 14.6764 8.88306 14.396 8.60294C14.1158 8.32303 13.6618 8.32312 13.3817 8.60314L8.81376 13.1702C8.61389 13.3702 8.45536 13.6075 8.34721 13.8687C8.23907 14.1299 8.18343 14.4099 8.18348 14.6926C8.18352 14.9753 8.23925 15.2552 8.34748 15.5164C8.45571 15.7775 8.61432 16.0148 8.81426 16.2147C9.0142 16.4146 9.25155 16.5731 9.51275 16.6813C9.77396 16.7894 10.0539 16.845 10.3366 16.845C10.6193 16.845 10.8993 16.7892 11.1604 16.681C11.4216 16.5728 11.6589 16.4142 11.8588 16.2142L17.9488 10.1252C18.6216 9.45223 18.9996 8.53951 18.9995 7.58786C18.9994 6.63621 18.6212 5.72357 17.9483 5.05071C17.2753 4.37786 16.3626 3.99991 15.4109 4C14.4593 4.00009 13.5466 4.37823 12.8738 5.05121L6.47976 11.4442L6.46576 11.4572C4.51076 13.4122 4.51076 16.5802 6.46576 18.5342C8.42076 20.4882 11.5888 20.4882 13.5438 18.5342L13.5568 18.5202L13.5578 18.5212L17.4151 14.6647C17.6954 14.3845 17.6953 13.93 17.4149 13.6499C17.1348 13.37 16.6808 13.3701 16.4007 13.65L12.5298 17.5192C11.1378 18.9112 8.87376 18.9112 7.48176 17.5192C7.14972 17.1864 6.88663 16.7914 6.70758 16.3568C6.52852 15.9221 6.43702 15.4564 6.43832 14.9863C6.43962 14.5162 6.5337 14.051 6.71516 13.6174C6.89661 13.1837 7.16189 12.7901 7.49576 12.4592L7.49476 12.4582L13.8898 6.06521C14.7288 5.22521 16.0948 5.22521 16.9348 6.06521C17.7748 6.90521 17.7738 8.27021 16.9348 9.10921L10.8448 15.1982C10.7085 15.3236 10.529 15.3914 10.3439 15.3876C10.1587 15.3838 9.98224 15.3086 9.85123 15.1777C9.72023 15.0468 9.64485 14.8704 9.64085 14.6853C9.63685 14.5001 9.70453 14.3206 9.82976 14.1842L14.3962 9.61774Z" fill="#8FA4B5"/>
                </svg></span>
                Document.txt
              </div>
            </div>
            <div className="proposal-info-property">
              <div className="proposal-info-title">hash sum</div>
              <div className="proposal-document-hash">
                0x55ff…9a4a
              </div>
            </div>
            <div className="proposal-info-property">
              <div className="proposal-info-title">Encryption key</div>
              <div className="proposal-document-hash">
                0x55ff…9a4a
              </div>
            </div>
          </div>
          <div className="proposal-info-block">
            <button type="button" className="sign-button">
              <svg width="24" height="24" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M18.834 7.16597C18.7815 7.11336 18.7192 7.07162 18.6505 7.04314C18.5819 7.01466 18.5083 7 18.434 7C18.3597 7 18.2861 7.01466 18.2175 7.04314C18.1488 7.07162 18.0865 7.11336 18.034 7.16597L9.587 15.632L5.965 12.002C5.91251 11.9494 5.85017 11.9076 5.78153 11.8791C5.71289 11.8507 5.63931 11.836 5.565 11.836C5.49068 11.836 5.4171 11.8507 5.34846 11.8791C5.27982 11.9076 5.21748 11.9494 5.165 12.002C5.05886 12.1084 4.99927 12.2526 4.99927 12.403C4.99927 12.5533 5.05886 12.6975 5.165 12.804L9.187 16.834C9.23934 16.8868 9.30164 16.9288 9.3703 16.9574C9.43896 16.986 9.51261 17.0008 9.587 17.0008C9.66138 17.0008 9.73503 16.986 9.80369 16.9574C9.87235 16.9288 9.93465 16.8868 9.987 16.834L18.834 7.96797C18.9401 7.8615 18.9997 7.7173 18.9997 7.56697C18.9997 7.41664 18.9401 7.27244 18.834 7.16597Z" fill="#08BEE5" stroke="#08BEE5" strokeWidth="0.5"/>
              </svg>
              Verify
            </button>
          </div>
        </div>
        <div className="main-card-block">
          <div className="proposal-card-title">
            <InfoTitle title={'employment proof'}/>
          </div>
          <div className="proposal-info-block row">
            <div className="employment-proof-manager-picture">

            </div>
            <div className="employment-proof-manager-info">
              <div className="employment-proof-manager-fullname">
                {fullName}
              </div>
              <div className="employment-proof-manager-position">
                Manager
              </div>
            </div>
          </div>
          <div className="proposal-info-block">
            <div className="proposal-info-property">
              <div className="proposal-info-title">Company</div>
              Company
            </div>
          </div>
          <div className="proposal-info-block more-button">
            More
          </div>
          <div className="proposal-info-block">
            <button type="button" className="sign-button">
              <svg width="24" height="24" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M18.834 7.16597C18.7815 7.11336 18.7192 7.07162 18.6505 7.04314C18.5819 7.01466 18.5083 7 18.434 7C18.3597 7 18.2861 7.01466 18.2175 7.04314C18.1488 7.07162 18.0865 7.11336 18.034 7.16597L9.587 15.632L5.965 12.002C5.91251 11.9494 5.85017 11.9076 5.78153 11.8791C5.71289 11.8507 5.63931 11.836 5.565 11.836C5.49068 11.836 5.4171 11.8507 5.34846 11.8791C5.27982 11.9076 5.21748 11.9494 5.165 12.002C5.05886 12.1084 4.99927 12.2526 4.99927 12.403C4.99927 12.5533 5.05886 12.6975 5.165 12.804L9.187 16.834C9.23934 16.8868 9.30164 16.9288 9.3703 16.9574C9.43896 16.986 9.51261 17.0008 9.587 17.0008C9.66138 17.0008 9.73503 16.986 9.80369 16.9574C9.87235 16.9288 9.93465 16.8868 9.987 16.834L18.834 7.96797C18.9401 7.8615 18.9997 7.7173 18.9997 7.56697C18.9997 7.41664 18.9401 7.27244 18.834 7.16597Z" fill="#08BEE5" stroke="#08BEE5" strokeWidth="0.5"/>
              </svg>
              Verify
            </button>
          </div>
        </div>
        <div className="main-card-block">
          <div className="proposal-card-title">
            <InfoTitle title={'manager id'}/>
          </div>
          <div className="proposal-info-block row">
            <div className="employment-proof-manager-picture">

            </div>
            <div className="employment-proof-manager-info">
              <div className="employment-proof-manager-fullname">
                {fullName}
              </div>
              <div className="employment-proof-manager-iso2">
                LT
              </div>
            </div>
          </div>
          <div className="proposal-info-block">
            <div className="proposal-info-property">
              <div className="proposal-info-title">Date of birth</div>
              2020-08-08
            </div>
            <div className="proposal-info-property">
              <div className="proposal-info-title">Citizenship</div>
              Lithuania
            </div>
          </div>
          <div className="proposal-info-block">
            <div className="proposal-info-property">
              <div className="proposal-info-title">Document number</div>
              BBXXXXXXX
            </div>
            <div className="proposal-info-property">
              <div className="proposal-info-title">Personal code</div>
              XXXXXXXXXXX
            </div>
            <div className="proposal-info-property">
              <div className="proposal-info-title">Date of expiry</div>
              2020-08-08
            </div>
          </div>
          <div className="proposal-info-block more-button">
            More
          </div>
          <div className="proposal-info-block">
            <button type="button" className="sign-button">
              <svg width="24" height="24" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M18.834 7.16597C18.7815 7.11336 18.7192 7.07162 18.6505 7.04314C18.5819 7.01466 18.5083 7 18.434 7C18.3597 7 18.2861 7.01466 18.2175 7.04314C18.1488 7.07162 18.0865 7.11336 18.034 7.16597L9.587 15.632L5.965 12.002C5.91251 11.9494 5.85017 11.9076 5.78153 11.8791C5.71289 11.8507 5.63931 11.836 5.565 11.836C5.49068 11.836 5.4171 11.8507 5.34846 11.8791C5.27982 11.9076 5.21748 11.9494 5.165 12.002C5.05886 12.1084 4.99927 12.2526 4.99927 12.403C4.99927 12.5533 5.05886 12.6975 5.165 12.804L9.187 16.834C9.23934 16.8868 9.30164 16.9288 9.3703 16.9574C9.43896 16.986 9.51261 17.0008 9.587 17.0008C9.66138 17.0008 9.73503 16.986 9.80369 16.9574C9.87235 16.9288 9.93465 16.8868 9.987 16.834L18.834 7.96797C18.9401 7.8615 18.9997 7.7173 18.9997 7.56697C18.9997 7.41664 18.9401 7.27244 18.834 7.16597Z" fill="#08BEE5" stroke="#08BEE5" strokeWidth="0.5"/>
              </svg>
              Verify
            </button>
          </div>
        </div>
      </div>
  )
};
