import React from 'react'
import {BackTray, InfoTitle, PageTitle} from './../../../components'
// import Slider from 'rc-slider';
import {Slider, TextInput} from '@aragon/ui'

import style from './style.css'
import 'rc-slider/assets/index.css';
import { debounce } from '../../../lib/debounce';

export const CreateIssue = () => {
  const [title, setTitle] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [countOfMembers, setCountOfMembers] = React.useState(0);
  const [countOfMinApproval, setCountOfMinApproval] = React.useState(0);

  //todo add debounce
  const handleChangeSliderMembers = debounce((countOfMembers) => {
    setCountOfMembers(Math.floor(countOfMembers * 100) / 100)
  }, 250);
  const handleChangeSliderMinApproval = (countOfMinApproval) => {
    setCountOfMinApproval(Math.floor(countOfMinApproval * 100) / 100);
    console.log(`Count of MinApproval`, countOfMinApproval);
  };

  const addornmentSetting = {
    width: 40,
    padding: 13,
  }

  return(
      <div className="main__container">
        <PageTitle name={'create issue'} />
        <BackTray />
        <div className="main-cards-blocks">
          <div className="main-card-block main-block">
            <div className="proposal-info-block">
              <InfoTitle title={'title'} />
              <TextInput 
                value={title}
                onChange={e => setTitle(e.target.value)}
                placeholder="Title"
                className="title-input"
              />
              <InfoTitle title={'description'} />
              <TextInput 
                value={description}
                onChange={e => setDescription(e.target.value)}
                placeholder="Text"
                className="create-issue-input issue-description-textarea"
              />
              <div className="create-issue-dates">
                <div className="create-issue-dates-element">
                  <InfoTitle title={'start date'} />
                  <div className="issue-dates-element-inputs">

                  </div>
                </div>
                <div className="create-issue-dates-element">
                  <InfoTitle title={'end date'} />
                  <div className="issue-dates-element-inputs">

                  </div>
                </div>
              </div>
            </div>
            <div className="proposal-info-block">
              <div className="support-block">
                <div className="support-block-header">
                  <InfoTitle title={'support'} />
                  <div className="support-block-tip">
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <rect width="14" height="14" rx="7" fill="#C8D7EA"/>
                      <path d="M6.70806 2.33337C5.26431 2.33337 4.08306 3.48021 4.08306 4.88196C4.07897 5.15146 4.33097 5.40229 4.60806 5.40229C4.88572 5.40229 5.13714 5.15146 5.13306 4.88196C5.13306 4.03146 5.83189 3.35304 6.70806 3.35304C7.58422 3.35304 8.28306 4.03146 8.28306 4.88137C8.28889 5.57962 7.91906 5.93721 7.40222 6.41087C7.14439 6.64771 6.85797 6.89504 6.61472 7.21821C6.37264 7.54137 6.18306 7.96196 6.18306 8.44962C6.17897 8.71912 6.43097 8.96996 6.70806 8.96996C6.98572 8.96996 7.23714 8.71912 7.23306 8.44962C7.23306 8.19821 7.30889 8.02321 7.46289 7.81787C7.61631 7.61312 7.85547 7.39554 8.12439 7.14937C8.66106 6.65646 9.33772 6.12679 9.33306 4.88196C9.33306 3.49012 8.14889 2.33337 6.70806 2.33337ZM6.70806 9.72421C6.32131 9.72421 6.00806 10.0281 6.00806 10.4038C6.00806 10.7789 6.32131 11.0834 6.70806 11.0834C7.09481 11.0834 7.40806 10.7789 7.40806 10.4038C7.40806 10.0287 7.09481 9.72421 6.70806 9.72421Z" fill="#8FA4B5"/>
                    </svg>
                  </div>
                </div>
                <div className="support-block-main">
                  <Slider 
                    value={countOfMembers}
                    onUpdate={handleChangeSliderMembers}
                  />
                  <TextInput 
                    adornment="%" 
                    adornmentPosition="end"
                    adornmentSettings={addornmentSetting}
                    value={countOfMembers}
                    onChange={e => handleChangeSliderMembers(e.target.value)}
                  />
                </div>
              </div>
              <div className="support-block">
                <div className="support-block-header">
                  <InfoTitle title={'minimal approval'} />
                  <div className="support-block-tip">
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <rect width="14" height="14" rx="7" fill="#C8D7EA"/>
                      <path d="M6.70806 2.33337C5.26431 2.33337 4.08306 3.48021 4.08306 4.88196C4.07897 5.15146 4.33097 5.40229 4.60806 5.40229C4.88572 5.40229 5.13714 5.15146 5.13306 4.88196C5.13306 4.03146 5.83189 3.35304 6.70806 3.35304C7.58422 3.35304 8.28306 4.03146 8.28306 4.88137C8.28889 5.57962 7.91906 5.93721 7.40222 6.41087C7.14439 6.64771 6.85797 6.89504 6.61472 7.21821C6.37264 7.54137 6.18306 7.96196 6.18306 8.44962C6.17897 8.71912 6.43097 8.96996 6.70806 8.96996C6.98572 8.96996 7.23714 8.71912 7.23306 8.44962C7.23306 8.19821 7.30889 8.02321 7.46289 7.81787C7.61631 7.61312 7.85547 7.39554 8.12439 7.14937C8.66106 6.65646 9.33772 6.12679 9.33306 4.88196C9.33306 3.49012 8.14889 2.33337 6.70806 2.33337ZM6.70806 9.72421C6.32131 9.72421 6.00806 10.0281 6.00806 10.4038C6.00806 10.7789 6.32131 11.0834 6.70806 11.0834C7.09481 11.0834 7.40806 10.7789 7.40806 10.4038C7.40806 10.0287 7.09481 9.72421 6.70806 9.72421Z" fill="#8FA4B5"/>
                    </svg>
                  </div>
                </div>
                <div className="support-block-main">
                <Slider 
                    value={countOfMinApproval}
                    onUpdate={handleChangeSliderMinApproval}
                  />
                  <TextInput 
                    adornment="%" 
                    adornmentPosition="end"
                    adornmentSettings={addornmentSetting}
                    value={countOfMinApproval}
                    onChange={handleChangeSliderMinApproval}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="main-card-block additional-block">

          </div>
        </div>
      </div>
  )
};
