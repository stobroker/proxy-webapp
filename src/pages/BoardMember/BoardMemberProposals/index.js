import React from 'react'
import {PageTitle, Proposal} from './../../../components'
import history from "../../../lib/routing";
import style from './style.css'
import { Link } from 'react-router-dom';

const proposals = [
  {
    id: 1,
    type: 'Purchase Contract Proxy',
    date: '2021-02-04',
    manager: 'James Tan',
    status: 'review',
  },
  {
    id: 2,
    type: 'Bank Account Opening Proxy',
    date: '2021-04-12',
    manager: 'Tanya James',
    status: 'complete',
  },
  {
    id: 3,
    type: 'Broker Service Proxy',
    date: '2020-04-29',
    manager: 'Dianne Russell',
    status: 'complete',
  }
];

export const BoardMemberProposals = () => {
  // const [...proposals] = React.useSelector(state => state.proposals)

    return(
      <div className="main__container" onClick={() => history.push('/create-proposal')}>
        <PageTitle name={'board member proposals'}/>
        <div className="board-member-proposals-container">
          <div className="board-member-proposals-container-header">
            <div className="proposal-id grey-medium fs-12">id</div>
            <div className="proposal-type grey-medium fs-12">type</div>
            <div className="proposal-date grey-medium fs-12">proposal date</div>
            <div className="proposal-manager grey-medium fs-12">manager</div>
            <div className="proposal-status grey-medium fs-12">status</div>
            <div className="proposal-details grey-medium fs-12"></div>

          </div>
          {proposals.map((element) => (
            // todo ??
            <Link>
              <Proposal
                {...element}
                key={element.id}  
              />
            </Link>
          ))}
        </div>
      </div>
    )
};

