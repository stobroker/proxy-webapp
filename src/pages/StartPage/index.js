import { getDisplayName } from '@aragon/ui/dist/getDisplayName-7f913e84'
import React from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { Link } from 'react-router-dom'
import { GridColumn } from 'semantic-ui-react'
import {setUserWallet} from './../../store/actions/user'

import style from './style.css'

export const StartPage = () => {
  const dispatch = useDispatch();

  let {userRole} = useSelector(state => state.user);

  const setWallet = (wallet) => {
    dispatch(setUserWallet(wallet));
  }

  let metamaskExists = false;

  // todo get userRole from backend
  // todo uncomment
  React.useEffect(async () => {
    if (typeof window.ethereum !== 'undefined') {
      metamaskExists = true;
      const response = await ethereum.request({ method: 'eth_requestAccounts' });
      setWallet(response[0])
    }
  }, []);

  // React.useEffect(() => {
  //   console.log('rerender')
  // }, [metamaskExists])

  //refactor later
  return(
    <div>
      <Link to="/create-proposal">Create Proposal</Link>
      <Link to="/proposal-page">Proposal</Link>
      <Link to="/board">Proposals board</Link>
      <Link to="/create-issue">Create Issue</Link>
      <Link to="/voting">Voting</Link>
      {/* {userRole === 'manager' ? <Redirect to="/create-proposal" /> : <Redirect to="/board" />} */}
      {/* {metamaskExists && <button>Connect metamask</button>}
      {(!metamaskExists) && <button>Install metamask</button>} */}
    </div>
  )
}