import React from 'react'
import {PageTitle, BackTray, InfoTitle, SubTitle, RequiredElement, AvailableCredential, ProvidedCredential} from './../../../components'
import Select from 'react-select'

import style from './style.css'

const proposalTypes = [
  {
    value: 'Purchase Contract Proxy',
    label: 'Purchase Contract Proxy'
  },
  {
    value: 'Bank Account Opening Proxy',
    label: 'Bank Account Opening Proxy'
  },
  {
    value: 'Broker Service Proxy',
    label: 'Broker Service Proxy'
  }
]

const timeValidity = [
  {
    value: '1month',
    label: '1 month',
  },
  {
    value: '6month',
    label: '6 month',
  },
  {
    value: '1year',
    label: '1 year',
  }
]

const requiredElements = [
  {
    title: 'Employment credential',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    isApproved: false,
  },
  {
    title: 'ID credential ',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    isApproved: true,
  }
]

const availableCredentials = [
  {
    title: 'ID',
    required: false,
  },
  {
    title: 'Employment credential',
    required: true,
  }
]

const providedCredentials = [
  {
    title: 'ID credential ',
    required: true,
  }
]

export const CreateProposal = () => {
  const [selectedType, setSelectedType] = React.useState(null);
  const [selectedValidity, setSelectedValidity] = React.useState(null);
  const [currentDescription, setCurrentDescription] = React.useState('');

  const handleChangeProposalType = (selectedType) => {
    setSelectedType(selectedType);
    console.log(`Selected type:`, selectedType);
  };
  const handleChangeValidity = (selectedValidity) => {
    setSelectedValidity(selectedValidity);
    console.log(`Selected validity: `, selectedValidity);
  };
  const handleChangeDescription = (currentDescription) => {
    setCurrentDescription(currentDescription);
    console.log(`Current description`, currentDescription);
  };


  return (
      <div className="main__container">
        <PageTitle name={'create proposal'} />
        <BackTray />
        <div className="main__form">
          <div className="form-general-info">
            <div className="type__period">
              <div className="form-type general-info">
                <InfoTitle title={'proposal type'}/>
                <Select
                    value={selectedType}
                    onChange={handleChangeProposalType}
                    options={proposalTypes}
                />
              </div>
              <div className="form-validity general-info">
                <InfoTitle title={'validity'} />
                <Select
                    value={selectedValidity}
                    onChange={handleChangeValidity}
                    options={timeValidity}
                />
              </div>
            </div>
            <div className="form-description general-info">
              <InfoTitle title={'description'}/>
              <textarea 
                placeholder="Text" 
                className="description-textarea"
                value={currentDescription}
                onChange={e => handleChangeDescription(e.target.value)}
              ></textarea>
            </div>
          </div>
          <div className="authorization-documents">
            <div className="auth-document__input">
              <InfoTitle title={'authorization documents'}/>
              <label htmlFor="fileInput1" className="load-file-button">
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.00014 3.33337C7.90006 3.33337 7.80408 3.37313 7.73332 3.44389C7.66256 3.51466 7.6228 3.61063 7.6228 3.71071V12.29C7.6228 12.3901 7.66256 12.4861 7.73332 12.5569C7.80408 12.6276 7.90006 12.6674 8.00014 12.6674C8.10021 12.6674 8.19619 12.6276 8.26695 12.5569C8.33772 12.4861 8.37747 12.3901 8.37747 12.29V3.71004C8.37729 3.61008 8.33746 3.51428 8.26672 3.44366C8.19597 3.37304 8.1001 3.33337 8.00014 3.33337Z" fill="#8FA4B5" stroke="#8FA4B5" strokeWidth="0.3"/>
                  <path d="M12.2893 7.62268H3.71068C3.6106 7.62268 3.51463 7.66244 3.44386 7.7332C3.3731 7.80396 3.33334 7.89994 3.33334 8.00001C3.33334 8.10009 3.3731 8.19607 3.44386 8.26683C3.51463 8.33759 3.6106 8.37735 3.71068 8.37735H12.29C12.3901 8.37735 12.4861 8.33759 12.5568 8.26683C12.6276 8.19607 12.6673 8.10009 12.6673 8.00001C12.6673 7.89994 12.6276 7.80396 12.5568 7.7332C12.4861 7.66244 12.3901 7.62268 12.29 7.62268H12.2893Z" fill="#8FA4B5" stroke="#8FA4B5" strokeWidth="0.3"/>
                </svg>
                Choose File
                <span>*</span>
              </label>
              <input type="file" className="file__input" id="fileInput1"/>
              <InfoTitle title={'additional documents'}/>
              <label htmlFor="fileInput2" className="load-file-button">
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.00014 3.33337C7.90006 3.33337 7.80408 3.37313 7.73332 3.44389C7.66256 3.51466 7.6228 3.61063 7.6228 3.71071V12.29C7.6228 12.3901 7.66256 12.4861 7.73332 12.5569C7.80408 12.6276 7.90006 12.6674 8.00014 12.6674C8.10021 12.6674 8.19619 12.6276 8.26695 12.5569C8.33772 12.4861 8.37747 12.3901 8.37747 12.29V3.71004C8.37729 3.61008 8.33746 3.51428 8.26672 3.44366C8.19597 3.37304 8.1001 3.33337 8.00014 3.33337Z" fill="#8FA4B5" stroke="#8FA4B5" strokeWidth="0.3"/>
                  <path d="M12.2893 7.62268H3.71068C3.6106 7.62268 3.51463 7.66244 3.44386 7.7332C3.3731 7.80396 3.33334 7.89994 3.33334 8.00001C3.33334 8.10009 3.3731 8.19607 3.44386 8.26683C3.51463 8.33759 3.6106 8.37735 3.71068 8.37735H12.29C12.3901 8.37735 12.4861 8.33759 12.5568 8.26683C12.6276 8.19607 12.6673 8.10009 12.6673 8.00001C12.6673 7.89994 12.6276 7.80396 12.5568 7.7332C12.4861 7.66244 12.3901 7.62268 12.29 7.62268H12.2893Z" fill="#8FA4B5" stroke="#8FA4B5" strokeWidth="0.3"/>
                </svg>
                Choose File
                <span>*</span>
              </label>
              <input type="file" className="file__input" id="fileInput2"/>
              <div className="file-info-card">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione corporis aperiam hic repellat blanditiis et illo qui voluptate officiis, cumque saepe nemo? Reiciendis dolorem, vero ratione repellendus quisquam reprehenderit blanditiis.
              </div>
              <button type="button" className="sign-button">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M18.834 7.16597C18.7815 7.11336 18.7192 7.07162 18.6505 7.04314C18.5819 7.01466 18.5083 7 18.434 7C18.3597 7 18.2861 7.01466 18.2175 7.04314C18.1488 7.07162 18.0865 7.11336 18.034 7.16597L9.587 15.632L5.965 12.002C5.91251 11.9494 5.85017 11.9076 5.78153 11.8791C5.71289 11.8507 5.63931 11.836 5.565 11.836C5.49068 11.836 5.4171 11.8507 5.34846 11.8791C5.27982 11.9076 5.21748 11.9494 5.165 12.002C5.05886 12.1084 4.99927 12.2526 4.99927 12.403C4.99927 12.5533 5.05886 12.6975 5.165 12.804L9.187 16.834C9.23934 16.8868 9.30164 16.9288 9.3703 16.9574C9.43896 16.986 9.51261 17.0008 9.587 17.0008C9.66138 17.0008 9.73503 16.986 9.80369 16.9574C9.87235 16.9288 9.93465 16.8868 9.987 16.834L18.834 7.96797C18.9401 7.8615 18.9997 7.7173 18.9997 7.56697C18.9997 7.41664 18.9401 7.27244 18.834 7.16597Z" fill="#08BEE5" stroke="#08BEE5" strokeWidth="0.5"/>
                </svg>
                Sign
              </button>
            </div>
          </div>
          <div className="required-credentials">
            {/* add length */}
            <SubTitle name={`required credentials (${requiredElements.length})`} />
            {requiredElements ?
                requiredElements.map((element) => (
                    <RequiredElement
                        {...element}
                        key={element.title}
                    />
                ))
                : document.getElementsByClassName('required-credentials')[0].classList.add('approve-hidden')
            }
          </div>
        </div>
        <div className="credentials-available-provided">
          <div className="credentials-available">
            <SubTitle name={`Available credentials (${availableCredentials.length})`} />
            {availableCredentials ? availableCredentials.map((element) => (
                <AvailableCredential
                    {...element}
                    key={element.title}
                />
            )) : document.getElementsByClassName('credentials-available')[0].classList.add('approve-hidden')}
          </div>
          <div className="credentials-provided">

            <SubTitle name={`Provided credentials (${providedCredentials.length})`} />
            {providedCredentials ? providedCredentials.map((element) => (
                <ProvidedCredential
                    {...element}
                    key={element.title}
                />
            )) : document.getElementsByClassName('credentials-provided')[0].classList.add('approve-hidden')}
          </div>
        </div>
        <div className="create-proposal-button-container">
          <button type="submit" className="create-proposal-button">
            Create
          </button>
        </div>
      </div>
  )
};
