const initialState = {
  title: '',
  validity: '',
  description: '',
  authorizationDocument: '',
  additionalDocuments: []
}

export const proposal = (state = initialState, action) => {
  switch(action.type){
    case 'SET_PROPOSAL_INFORMATION':{
      const {title, validity, description} = action.payload;
      return {
        ...state,
        title: title,
        validity: validity,
        description: description,
      }
    }
    default: return {
      ...state
    }
  }
}
