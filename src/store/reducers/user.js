const initialState = {
  userWallet: '',
  userRole: 'manager'
}

export const user = (state = initialState, action) => {
  switch(action.type) {
    case 'SET_USER_WALLET':
      return {
        userWallet: action.payload,
      }
    case 'SET_USER_ROLE':
        return {
          ...state,
          userRole: action.payload,
        }
    default:
      return state
  }
}