export const setProposalInformation = (objProposal) => ({
  type: 'SET_PROPOSAL_INFORMATION',
  payload: objProposal,
});