export const setUserWallet = (wallet) => ({
  type: 'SET_USER_WALLET',
  payload: wallet,
});

export const setUserInformation = (role) => ({
  type: 'SET_USER_ROLE',
  payload: role,
});