import React from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import history from './lib/routing'
import {CreateProposal, BoardMemberProposals, ProposalPage, CreateIssue, VerifierPage, VotingPage, StartPage} from './pages'

export const Routes = () => (
    <Router history={history}>
        <Switch>
            <Route exact path="/" component={StartPage}/>
            <Route path="/create-proposal" component={CreateProposal}/>
            <Route exact path="/proposal-page" component={ProposalPage}/>
            <Route exact path="/create-issue" component={CreateIssue}/>
            <Route exact path="/voting" component={VotingPage}/>
            <Route exact path="/verifier" component={VerifierPage}/>
            <Route exact path="/board" component={BoardMemberProposals}/>
        </Switch>
    </Router>
);
