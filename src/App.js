import React from 'react';
import styled from "styled-components";
import { Main } from '@aragon/ui'
// import {Routes} from "./routes";
import {Header, LeftBar} from './components'

import { Route, Switch } from 'react-router-dom'

import {CreateProposal, BoardMemberProposals, ProposalPage, CreateIssue, VerifierPage, VotingPage, StartPage} from './pages'

import './style.css';

const App = () => {
  return (
    <Main layout={false}>
      <Header />
      <Main2>
        <LeftBar />
        <Container>
           {/* <Routes/> */}
           <Switch>
            <Route exact path="/" component={StartPage}/>
            <Route path="/create-proposal" component={CreateProposal}/>
            <Route exact path="/proposal-page" component={ProposalPage}/>
            <Route exact path="/create-issue" component={CreateIssue}/>
            <Route exact path="/voting" component={VotingPage}/>
            <Route exact path="/verifier" component={VerifierPage}/>
            <Route exact path="/board" component={BoardMemberProposals}/>
        </Switch>
        </Container>
      </Main2>
    </Main>
  );
};

export default App;

const Main2 = styled.main`
  display: flex;
`;
const Container = styled.div`
  width: 100%;
`;
