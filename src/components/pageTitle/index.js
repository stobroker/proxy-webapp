import React from 'react'

import style from './style.css'

export default function PageTitle({name}){
  return(
    <div className="page-title">{name}</div>
  )
}