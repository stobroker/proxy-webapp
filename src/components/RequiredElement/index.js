import React from 'react'

import style from './style.css'

export default function RequiredElement({title, description, isApproved}){
  return(
    <div className="required-element">
      <div className="required-element-picture">
        <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect width="32" height="32" rx="6" fill="url(#paint0_linear)"/>
        <defs>
        <linearGradient id="paint0_linear" x1="2.81081" y1="-78.4" x2="-31.3449" y2="13.5576" gradientUnits="userSpaceOnUse">
        <stop stopColor="#32FFF5"/>
        <stop offset="1" stopColor="#08BEE5"/>
        </linearGradient>
        </defs>
        </svg>
      </div>
      <div className="required-element-title">
        {title} 
      </div>
      <div className="required-element-description">
        {description}
      </div>
      <div className={isApproved ? 'approve-show' : 'approve-hidden'}>
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="11.9999" cy="12" r="12" fill="#2CC68F"/>
          <path d="M16.5559 8.77715C16.5209 8.74208 16.4793 8.71425 16.4335 8.69526C16.3878 8.67628 16.3387 8.6665 16.2892 8.6665C16.2396 8.6665 16.1906 8.67628 16.1448 8.69526C16.0991 8.71425 16.0575 8.74208 16.0225 8.77715L10.3912 14.4212L7.97652 12.0012C7.94153 11.9661 7.89997 11.9383 7.85421 11.9193C7.80845 11.9003 7.7594 11.8905 7.70985 11.8905C7.66031 11.8905 7.61126 11.9003 7.5655 11.9193C7.51974 11.9383 7.47818 11.9661 7.44319 12.0012C7.37243 12.0721 7.3327 12.1683 7.3327 12.2685C7.3327 12.3687 7.37243 12.4648 7.44319 12.5358L10.1245 15.2225C10.1594 15.2577 10.201 15.2857 10.2467 15.3048C10.2925 15.3239 10.3416 15.3337 10.3912 15.3337C10.4408 15.3337 10.4899 15.3239 10.5357 15.3048C10.5814 15.2857 10.623 15.2577 10.6579 15.2225L16.5559 9.31182C16.6266 9.24084 16.6663 9.1447 16.6663 9.04448C16.6663 8.94426 16.6266 8.84813 16.5559 8.77715Z" fill="white" stroke="white" strokeWidth="0.5"/>
        </svg>
      </div>
    </div>
  )
}