import React from 'react'

import style from './style.css'

export default function SubTitle({name}){
  return(
    <div className="subtitle">{name}</div>
  )
}