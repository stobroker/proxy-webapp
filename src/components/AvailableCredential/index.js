import React from 'react'

import style from './style.css'

export default function AvailableCredentials({title, required}){
  return (
    <div className="available-credential-element">
      <div className="available-credential-main-info">
      <div className="available-credential-picture">
      <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect width="32" height="32" rx="6" fill="url(#paint0_linear)"/>
        <defs>
        <linearGradient id="paint0_linear" x1="2.81081" y1="-78.4" x2="-31.3449" y2="13.5576" gradientUnits="userSpaceOnUse">
        <stop stopColor="#32FFF5"/>
        <stop offset="1" stopColor="#08BEE5"/>
        </linearGradient>
        </defs>
        </svg>
      </div>
      <div className="available-credential-title">
        {title}
        <span className={required ? 'required-show' : 'required-hidden'}>Required</span>
      </div>
      <div className="available-credential-link">
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.902 12.17C16.8171 12.17 16.733 12.1867 16.6546 12.2193C16.5761 12.2518 16.5049 12.2995 16.4449 12.3596C16.3849 12.4197 16.3373 12.491 16.3049 12.5695C16.2725 12.648 16.2559 12.7321 16.256 12.817V17.72C16.2555 17.9818 16.1512 18.2327 15.966 18.4177C15.7808 18.6027 15.5298 18.7067 15.268 18.707H6.28C6.01839 18.7065 5.76765 18.6023 5.58267 18.4173C5.39769 18.2323 5.29353 17.9816 5.293 17.72V8.732C5.293 8.187 5.736 7.744 6.28 7.744H11.183C11.3545 7.744 11.5189 7.67589 11.6401 7.55464C11.7614 7.4334 11.8295 7.26896 11.8295 7.0975C11.8295 6.92604 11.7614 6.7616 11.6401 6.64036C11.5189 6.51911 11.3545 6.451 11.183 6.451H6.28C5.67555 6.45179 5.09608 6.69226 4.66867 7.11967C4.24126 7.54708 4.00079 8.12655 4 8.731V17.72C4.00079 18.3244 4.24126 18.9039 4.66867 19.3313C5.09608 19.7587 5.67555 19.9992 6.28 20H15.268C15.8724 19.9992 16.4519 19.7587 16.8793 19.3313C17.3067 18.9039 17.5472 18.3244 17.548 17.72V12.817C17.548 12.6458 17.4801 12.4817 17.3591 12.3606C17.2382 12.2394 17.0742 12.1713 16.903 12.171L16.902 12.17ZM19.354 4H14.45C14.2785 4 14.1141 4.06811 13.9929 4.18936C13.8716 4.3106 13.8035 4.47504 13.8035 4.6465C13.8035 4.81796 13.8716 4.9824 13.9929 5.10364C14.1141 5.22489 14.2785 5.293 14.45 5.293H18.706V9.549C18.706 9.72046 18.7741 9.8849 18.8954 10.0061C19.0166 10.1274 19.181 10.1955 19.3525 10.1955C19.524 10.1955 19.6884 10.1274 19.8096 10.0061C19.9309 9.8849 19.999 9.72046 19.999 9.549V4.646C19.999 4.47484 19.9311 4.31068 19.8101 4.18956C19.6892 4.06844 19.5252 4.00026 19.354 4Z" fill="#8FA4B5"/>
        <path d="M19.81 4.19002C19.6889 4.06905 19.5247 4.0011 19.3535 4.0011C19.1823 4.0011 19.0181 4.06905 18.897 4.19002L9.90899 13.178C9.78779 13.2992 9.7197 13.4636 9.7197 13.635C9.7197 13.8064 9.78779 13.9708 9.90899 14.092C10.0302 14.2132 10.1946 14.2813 10.366 14.2813C10.5374 14.2813 10.7018 14.2132 10.823 14.092L19.811 5.10402C19.871 5.04402 19.9187 4.97278 19.9512 4.89437C19.9837 4.81595 20.0004 4.7319 20.0004 4.64702C20.0004 4.56213 19.9837 4.47808 19.9512 4.39967C19.9187 4.32125 19.871 4.25001 19.811 4.19002H19.81Z" fill="#8FA4B5"/>
      </svg>
      </div>
      </div>
      <div className="available-credential-add">
      <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g filter="url(#filter0_d)">
        <path fillRule="evenodd" clipRule="evenodd" d="M4 7C4 4.79086 5.79086 3 8 3L32 3C34.2091 3 36 4.79086 36 7V31C36 33.2091 34.2091 35 32 35H8C5.79086 35 4 33.2091 4 31V7Z" fill="white"/>
        <path fillRule="evenodd" clipRule="evenodd" d="M4 7C4 4.79086 5.79086 3 8 3L32 3C34.2091 3 36 4.79086 36 7V31C36 33.2091 34.2091 35 32 35H8C5.79086 35 4 33.2091 4 31V7Z" stroke="#DDE4E9"/>
        </g>
        <path d="M20 12C19.8499 12 19.7059 12.0596 19.5998 12.1658C19.4936 12.2719 19.434 12.4159 19.434 12.566V25.435C19.434 25.5851 19.4936 25.7291 19.5998 25.8352C19.7059 25.9414 19.8499 26.001 20 26.001C20.1501 26.001 20.2941 25.9414 20.4002 25.8352C20.5064 25.7291 20.566 25.5851 20.566 25.435V12.565C20.5657 12.4151 20.506 12.2714 20.3999 12.1654C20.2938 12.0595 20.1499 12 20 12Z" fill="#8FA4B5" stroke="#8FA4B5" strokeWidth="0.3"/>
        <path d="M26.434 18.434H13.566C13.4159 18.434 13.2719 18.4936 13.1658 18.5997C13.0596 18.7059 13 18.8498 13 19C13 19.1501 13.0596 19.294 13.1658 19.4002C13.2719 19.5063 13.4159 19.566 13.566 19.566H26.435C26.5851 19.566 26.7291 19.5063 26.8352 19.4002C26.9414 19.294 27.001 19.1501 27.001 19C27.001 18.8498 26.9414 18.7059 26.8352 18.5997C26.7291 18.4936 26.5851 18.434 26.435 18.434H26.434Z" fill="#8FA4B5" stroke="#8FA4B5" strokeWidth="0.3"/>
        <defs>
        <filter id="filter0_d" x="0.5" y="0.5" width="39" height="39" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
        <feFlood floodOpacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
        <feOffset dy="1"/>
        <feGaussianBlur stdDeviation="1.5"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
        </filter>
        </defs>
      </svg>
      </div>
    </div>
  )
}