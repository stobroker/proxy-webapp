import React from 'react'

import style from './style.css'

export default function InfoTitle({title}) {
  return (
    <div className="info-title">{title}</div>
  );
}