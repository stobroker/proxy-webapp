import React from 'react'

import style from './style.css'

export default function Proposal({id, type, manager, status, date}){
  return(
    <div className="board-member-proposal">
      <div className="proposal-id grey-dark fs-14">{id}</div>
      <div className="proposal-type grey-dark fs-14">{type}</div>
      <div className="proposal-date grey-dark fs-14">{date}</div>
      <div className="proposal-manager grey-dark fs-14">{manager}</div>
      <div className='proposal-status grey-dark fs-14 '>
        <span className={status === 'review' ? 'review-status' : 'complete-status'}>{status === 'review' ? 'For review' : 'Complete'}</span>
      </div>
      <div className="proposal-details grey-dark fs-14">
        <span className="details-btn">Details</span>
      </div>
    </div>
  )
}